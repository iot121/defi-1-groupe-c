<!DOCTYPE html>
<HTML lang="fr">
	<HEAD>
			<meta charset="UTF-8">
			<TITLE>
				Admin - RESA 2020
			</TITLE>
			  <link href="theme.css" rel="stylesheet">
	</HEAD>
	<BODY>
		<NAV>
			<div id="image_head"></div>
		</NAV>
		<SECTION>
			<h1>Service Réservation ICAM - Accès administrateur - Bienvenue !</h1>
			<p>
				<button id="deco" onclick="window.location.href ='main.php'">Déconnexion </button>
				<h3>Recherche de disponibilité d'équipement</br></h3> 
				<form method= "GET"  action = "search.php">
					<label id = "lname">Nom ou code de l'équipement :</label> 					
					<input type="Nom de l'équipement" id="sname" NAME="sname" />
					<label id = "ldate">Date de disponibilité :</label> 
					<input type="date" id="sdate" NAME="sdate"/>
					<input type="submit" value="Rechercher" id="lsubmit"/> <br/> </br>     <!-- donnez le type du bouton permettant d'envoyer l'information saisie -->
				</form>
				<h3>Service édition d'équipement</br></h3> 
				<form method= "GET"  action = "edit.php">
					<label id = "lname">Nom ou code de l'équipement :</label> 					
					<input type="Nom de l'équipement" id="sname" NAME="sname" />
					<input type="submit" value="Rechercher" id="lsubmit"/>
					<label id = "lname">(Pour ajouter un nouvel appareil, laisser ce champ vide)</label> <br/>      <!-- donnez le type du bouton permettant d'envoyer l'information saisie -->
				</form>
			</p>
			<font face="Arial">
        <?php
        
			$db = new mysqli('app.icam.fr','defi1groupeC','3adhgjHGRnRu6d7a','defi1groupeC');
			if ($db->connect_error)
				die("Connection echoué à la base de données:{$db -> connect_error}");
		?>
        <table border="5" cellspacing="2" cellpadding="5" align="center">
				<tr>
			<?php
			$req = $db->query("SELECT COLUMN_NAME FROM  information_schema.COLUMNS WHERE TABLE_NAME LIKE '%Resa%'");
			while ($rows = $req->fetch_assoc())
			{
			?>
                
                    <th><?php echo ($rows['COLUMN_NAME']);?></th>
                
			<?php
			}
			?>	
				</tr>
            <?php
            $res = $db->query('SELECT * from Resa');
			while ($row = $res->fetch_assoc())
			{
            ?>
                <tr>
				
			<?php	foreach ($row as $col => $value)
				echo "<td align='center'>" . $value . "</td>";
             ?>       
                </tr>
			<?php
			}
			?>
        </table>
	</font>
		</SECTION>	
		<FOOTER>
			<p>Fait avec <3 par Alexis, Flavien et Maxime</p>
		</FOOTER>
	</BODY>
</HTML>

